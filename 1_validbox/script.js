'use strict';

const registrationSubmit = document.getElementById('registrationSubmit');
const red = '#CB2424';
let checked = true;

function makeWarning(isWarning, block, text = 'Поле обязательно для заполнения') {
  const content = document.querySelector(`#${block} .formblock-textfield`);
  const star = document.querySelector(`#${block} .star`);
  const warningLabel = document.querySelector(`#${block} .warning`);
  const checkboxFlag = document.querySelector(`#${block} .checkbox-point`);

  if (isWarning) {
    checked = false;
    star.style.color = red;
    warningLabel.style.opacity = '100%';
    warningLabel.innerText = text;
    if (block !== 'agreement') {
      content.style.borderColor = red;
      content.value = '';
    }
    if (block === 'agreement') {
      checkboxFlag.style.borderColor = red;
    }
  } else {
    star.style.color = '';
    warningLabel.style.opacity = '';
    warningLabel.innerText = text;
    if (block !== 'agreement') {
      content.style.borderColor = '';
    }
    if (block === 'agreement') {
      checkboxFlag.style.borderColor = '';
    }
  }
}

function eraseBlocks() {
  const mail = document.querySelector('#mail .formblock-textfield');
  const password = document.querySelector('#password .formblock-textfield');
  const checkbox = document.querySelector('#agreement .checkbox-wrapped');
  mail.value = '';
  password.value = '';
  checkbox.checked = false;
}

function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

function checkEmail() {
  const mail = document.querySelector('#mail .formblock-textfield');
  if (!(mail.value)) {
    makeWarning(true, 'mail');
  } else if (!(validateEmail(mail.value))) {
    makeWarning(true, 'mail', 'Email невалидный');
  } else {
    makeWarning(false, 'mail');
  }
}

function checkPassword() {
  const password = document.querySelector('#password .formblock-textfield');
  if (!(password.value)) {
    makeWarning(true, 'password');
  } else if (password.value.length < 8) {
    makeWarning(true, 'password', 'Пароль должен содержать как минимум 8 символов');
  } else {
    makeWarning(false, 'password');
  }
}

function checkAgreement() {
  const checkbox = document.querySelector('#agreement .checkbox-wrapped');
  if (!checkbox.checked) {
    makeWarning(true, 'agreement');
  } else {
    makeWarning(false, 'agreement');
  }
}

registrationSubmit.addEventListener('click', (event) => {
  const mail = document.querySelector('#mail .formblock-textfield');
  const pass = document.querySelector('#password .formblock-textfield');
  const personRegInf = {
    email: mail.value,
    password: pass.value,
  };
  event.preventDefault();
  event.stopPropagation();
  checkEmail();
  checkPassword();
  checkAgreement();
  if (checked) {
    console.log(personRegInf);
    eraseBlocks();
  }
  checked = true;
});
